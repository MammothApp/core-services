defmodule MammothWeb.Router do
  use MammothWeb, :router

  pipeline :api do
    plug(:accepts, ["json"])
  end

  scope "/api", MammothWeb do
    pipe_through(:api)
  end
end
