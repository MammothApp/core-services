# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# Configures the endpoint
config :mammoth, MammothWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "YhfqGK/6kyP3evtX4p7Z3uHAWOHhnJ4VL0LPuR+bRHd6N4OMSeQjHVzMFYwm95RN",
  render_errors: [view: MammothWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: Mammoth.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
